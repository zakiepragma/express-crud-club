import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const getClubs = async (req, res) => {
  try {
    const response = await prisma.club.findMany({
      orderBy: {
        point: "desc",
      },
    });
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getClubById = async (req, res) => {
  try {
    const response = await prisma.club.findUnique({
      where: {
        id: Number(req.params.id),
      },
    });
    res.status(200).json(response);
  } catch (error) {
    res.status(404).json({ msg: error.message });
  }
};

export const createClub = async (req, res) => {
  const { name, point } = req.body;
  try {
    const product = await prisma.club.create({
      data: {
        name: name,
        point: point,
      },
    });
    res.status(201).json(product);
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const updateClub = async (req, res) => {
  const { name, point } = req.body;
  try {
    const product = await prisma.club.update({
      where: { id: Number(req.params.id) },
      data: {
        name: name,
        point: point,
      },
    });
    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

export const deleteClub = async (req, res) => {
  try {
    const product = await prisma.club.delete({
      where: { id: Number(req.params.id) },
    });
    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};
