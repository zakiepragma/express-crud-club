import express from "express";
import {
  getClubs,
  getClubById,
  createClub,
  updateClub,
  deleteClub,
} from "../controllers/ClubController.js";

const router = express.Router();

router.get("/clubs", getClubs);
router.get("/clubs/:id", getClubById);
router.post("/clubs", createClub);
router.patch("/clubs/:id", updateClub);
router.delete("/clubs/:id", deleteClub);

export default router;
